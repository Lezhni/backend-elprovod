<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'elprovod');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9m-OWP3J5>b4fra4z/y:qdh#t9:qBA-|6lhO;q>=1-ump#,|_.JgNS]#7Xp?OZoe');
define('SECURE_AUTH_KEY',  'h.eI2b.JG3VEO3h@r6K!aIoX@xMt*[-W0k1ZBFT,+=b4/#=0=R%|.5P<BOa+ekoY');
define('LOGGED_IN_KEY',    '%2w`9L&,v@w)5A>T`H*L|>%;CJ|kaO<udF)w SnCy{zvj(,uS&SmZ^n*T}`j8=VJ');
define('NONCE_KEY',        '%rk>2?@DVhPXl>?L,k|b3U~rDGxxX%--w~p2v%Jz_Ii=sDcb9>pl>r(:gv^+j2}Z');
define('AUTH_SALT',        'Mk~p`7RhxSl|Z$<dDfJ.*z8u!x5vmDFr2!nY`h}u>n{hz={2PfcZ78t`3*3#E+oK');
define('SECURE_AUTH_SALT', 'CC:[C21:l$?P3_JvJV89|u/PaW0USrY6zdL+t?/|phy>k?^v5C#NPK9KeOr FLTr');
define('LOGGED_IN_SALT',   'CK|*BtdBmX.]6%.IFAc<t&fp+`fDTb^p1]9r#/&`ug`s6YW9O+{ ;w5&)bv9l:Xj');
define('NONCE_SALT',       'uNe|B$BLx?;qh(o<|DFofU#O*YN;UuQ}I9d/XHB/{AJ|SaP|(>cS>;$|Z#z:z1vD');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'el_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WPLANG', 'ru_RU');
define('WP_MEMORY_LIMIT', '86m');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
