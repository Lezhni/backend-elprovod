module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            production: {
                files: {
                    "wp-content/themes/elprovod/css/raw/style.css": "wp-content/themes/elprovod/less/style.less"
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 10 versions', 'Firefox > 15', 'Opera >= 12']
            },
            build: {
                src: 'wp-content/themes/elprovod/css/raw/style.css',
                dest: 'wp-content/themes/elprovod/css/raw/style.css'
            }
        },
        concat_css: {
            all: {
              src: ["wp-content/themes/elprovod/css/raw/*.css"],
              dest: "wp-content/themes/elprovod/css/style.css"
            }
        },
        cssmin: {
            target: {
                files: {
                    'wp-content/themes/elprovod/css/style.min.css': 'wp-content/themes/elprovod/css/style.css'
                }
            }
        },
        concat: {
            dist: {
                src: [
                    'wp-content/themes/elprovod/js/raw/*.js'
                ],
                dest: 'wp-content/themes/elprovod/js/main.js',
                separator: ''
            }
        },
        uglify: {
            build: {
                src: 'wp-content/themes/elprovod/js/main.js',
                dest: 'wp-content/themes/elprovod/js/main.min.js'
            }
        },
        imagemin : {
            dynamic: {
                files: [{
                    cwd: 'wp-content/themes/elprovod/img/raw/',
                    src: ['**/*.{png,jpg,gif,svg}'],
                    dest: 'wp-content/themes/elprovod/img/'
                }],
                optimizationLevel: 5
            }
        },
        watch: {
            styles: {
                files: ['wp-content/themes/elprovod/less/*.less'],
                tasks: ['less', 'autoprefixer','concat_css', 'cssmin'],
                options: {
                    nospawn: true
                }
            },
            scripts: {
                files: ['wp-content/themes/elprovod/js/raw/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('images', ['imagemin']);
    grunt.registerTask('all', ['less', 'autoprefixer', 'concat_css', 'cssmin', 'concat', 'uglify', 'imagemin']);
}
