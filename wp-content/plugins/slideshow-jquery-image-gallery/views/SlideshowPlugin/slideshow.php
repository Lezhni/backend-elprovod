<?php if ($data instanceof stdClass) : ?>
	
	<?php

	if (is_array($data->slides) && count($data->slides) > 0)
	{
		$i = 0;

		for ($i; $i < count($data->slides); $i++)
		{
			for ($i; $i < count($data->slides); $i++)
			{
				$slideData             = new stdClass();
				$slideData->properties = $data->slides[$i];

				SlideshowPluginMain::outputView('SlideshowPluginSlideshowSlide' . DIRECTORY_SEPARATOR . 'frontend_' . $data->slides[$i]['type'] . '.php', $slideData);

				if (($i + 1) % $data->settings['slidesPerView'] == 0)
				{
					break;
				}
			}
		}
	}

	?>

<?php endif; ?>