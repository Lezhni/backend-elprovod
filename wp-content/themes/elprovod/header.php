<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
	<![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo( 'home' ); ?>/favicon.ico" />
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<aside id="callback" class="mfp-hide">
		<strong class="form-title">Обратный звонок</strong>
		<?php echo do_shortcode( '[contact-form-7 id="90" title="Обратный звонок"]' ); ?>
	</aside>
	<header role="banner">
		<div class="container header-top">
			<a href="<?php bloginfo( 'url' ); ?>" class="logo to-left">
				<img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" alt="">
			</a>

			<?php get_search_form(); ?>

			<a href="mailto:<?php echo get_option( 'contact_email' ); ?>" class="header-phone header-email to-right"><?php echo get_option( 'contact_email' ); ?></a>
			<?php $formated_phone = preg_replace('/\D/', '', get_option('contact_phone') ); ?>
			<a href="callto:<?php echo $formated_phone; ?>" class="header-phone to-right"><?php echo get_option( 'contact_phone' ); ?></a>
		</div>
		<nav class="container header-menu">
			<?php wp_nav_menu( array(
				'theme_location' => 'header-menu'
			) ); ?>
		</nav>
	</header>
	<main role="main">
