<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section <?php post_class( 'single-page' ); ?>>
  <div class="container">
    <section class="wide-col">
      <nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <?php if ( function_exists('bcn_display') ) bcn_display(); ?>
      </nav>
      <h1><?php the_title(); ?></h1>
      <section class="page-content">
        <?php the_content(); ?>
      </section>
    </section>

    <?php get_sidebar(); ?>

  </div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?>
