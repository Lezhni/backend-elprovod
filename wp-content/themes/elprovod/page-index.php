<?php get_header(); ?>

<section class="main-slideshow">
	<?php do_action( 'slideshow_deploy', '6' ); ?>
</section>
<?php
	// Get featured wires
	$featured_wires = new WP_Query( array(
		'numberposts'	=> -1,
		'order' => 'ASC',
		'post_type'		=> 'product',
		'meta_key'		=> 'show_on_index',
		'meta_value'	=> 1
	) );
?>
<?php if ( $featured_wires->have_posts() ) : ?>

	<section class="our-cables">
		<div class="container">
			<h2>Продукция завода</h2>
			<div class="block-cols block-cols-4">
			<?php while ( $featured_wires->have_posts() ) : $featured_wires->the_post(); ?>
				<figure class="block-col">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail(); ?>
						<figcaption><?php the_title(); ?></figcaption>
					</a>
				</figure>
			<?php endwhile; ?>
			</div>
		</div>
	</section>

<?php wp_reset_query(); ?>
<?php endif; ?>
<section class="our-history">
	<div class="container">
		<section class="wide-col">
			<h2>История завода «Электропровод»<span>с 1785 года!</span></h2>
			<article>
				<?php $history = get_page( 13 ); ?>
				<img src="<?php bloginfo( 'template_url' ); ?>/img/history-tmp.jpg" class="to-left" alt="">
				<p><?php echo wp_trim_words( $history->post_content, 50 ); ?></p>
				<a href="<?php echo get_permalink( 13 ); ?>" class="button button-grey">Подробнее</a>
			</article>
		</section>
		<aside class="narrow-col">
			<div class="aside-col contacts-col">
				<?php echo get_option( 'contacts' ); ?>
				<a href="#" class="button button-red-fill">Заказать звонок</a>
			</div>
		</aside>
	</div>
</section>
<?php // Get news
	$news = new WP_Query( array(
		'post_type' => 'post',
		'category_name' => 'news',
		'posts_per_page' => 4
	) );
?>
<?php if ( $news->have_posts() ) : ?>

<section class="our-news">
	<div class="container">
		<div class="block-cols block-cols-4">
			<?php while ( $news->have_posts() ) : $news->the_post(); ?>

			<article <?php post_class('block-col our-news-single'); ?>>
				<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php the_permalink(); ?>" class="our-news-img">
					<?php the_post_thumbnail(); ?>
				</a>
				<?php endif; ?>
				<h3 class="our-news-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<div class="our-news-bar">
					<div class="our-news-cat">
						<?php the_category(); ?>
					</div>
					<date class="our-news-date"><?php the_date( 'd.m.Y' ); ?></date>
				</div>
				<div class="our-news-desc">
					<?php the_excerpt(); ?>
				</div>
			</article>

		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<?php endif; ?>
<section class="about-us">
	<div class="container">
		<section class="about-us-catalog">
			<img src="<?php bloginfo( 'template_url' ); ?>/img/cables-bg.jpg" alt="">
			<article class="about-us-overlay to-right">
				<?php while ( have_posts() ) : the_post(); ?>

				<h3><?php the_title(); ?></h3>
				<p><?php the_content(); ?></p>

				<?php endwhile; ?>
				<a href="/product" class="button button-red-fill">Перейти в каталог</a>
			</article>
		</section>
		<?php if (get_field( 'about_left' ) || get_field( 'about_right' )) : ?>

		<div class="about-us-factory">
			<h4>О заводе "Электропровод"</h4>
			<div class="block-cols block-cols-2">
				<div class="article block-col"><?php echo get_field( 'about_left' ); ?></div>
				<div class="article block-col"><?php echo get_field( 'about_right' ); ?></div>
			</div>
		</div>

		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>
