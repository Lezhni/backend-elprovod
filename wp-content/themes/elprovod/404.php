<?php get_header(); ?>

<section <?php post_class( '404-page single-page' ); ?>>
  <div class="container">
    <section class="wide-col">
      <nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <?php if ( function_exists('bcn_display') ) bcn_display(); ?>
      </nav>
      <h1>Страница не найдена</h1>
      <section class="page-content">
        <p>К сожалению, страница с таким адресом не существует. Скорее всего, Вы ошиблись при вводе адреса, либо перешли по неработающей ссылке. Попробуйте следующие варианты:</p>
        <ul>
          <li>Обновите страницу;</li>
          <li>Вернитесь на <a href="?php bloginfo( 'url' ); ?>">главную страницу</a>;</li>
          <li>Попробуйте воспользоваться поиском для нахождения страницы.</li>
        </ul>
        <p>Нам действительно жаль, что так получилось<p>
      </section>
    </section>

    <?php get_sidebar(); ?>

  </div>
</section>

<?php get_footer(); ?>
