<?php

// Connect custom css
if ( !is_admin() ) {
    wp_register_style( 'elprovod', get_bloginfo('stylesheet_directory') . '/css/style.min.css', false, 0.1 );
    wp_enqueue_style( 'elprovod' );
}

// Hide admin bar
add_filter( 'show_admin_bar', '__return_false' );

// Connect menus
function custom_menus() {
    register_nav_menu( 'header-menu', __('Верхнее меню') );
    register_nav_menu( 'footer-menu-1', __('Нижнее меню 1') );
    register_nav_menu( 'footer-menu-2', __('Нижнее меню 2') );
}
add_action( 'init', 'custom_menus' );

// Add tag support to pages
function tags_support_all() {
    register_taxonomy_for_object_type( 'post_tag', 'page' );
}

// Ensure all tags are included in queries
function tags_support_query( $wp_query ) {
    if ( $wp_query->get('tag') ) $wp_query->set( 'post_type', 'any' );
}

// Tag hooks
add_action( 'init', 'tags_support_all' );
add_action( 'pre_get_posts', 'tags_support_query' );

// Add page type for catalog
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'product',
    array(
      'labels' => array(
        'name' => 'Продукция',
        'singular_name' => 'Продукт'
      ),
      'public' => true,
      'taxonomies' => array( 'category' ),
      'has_archive' => true
    )
  );
}

// Removes from admin menu
add_action( 'admin_menu', 'remove_comments_admin' );
function remove_comments_admin() {
    remove_menu_page( 'edit-comments.php' );
}

// Removes from post and pages
add_action('init', 'remove_comment_support', 100);
function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function removes_comments_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'removes_comments_bar' );

// Add sidebar
function sidebar_widgets_init() {
    register_sidebar( array(
        'name'          => 'Боковая панель',
        'id'            => 'right_sidebar',
        'description' => 'Основная боковая панель для размещения виджетов (меню, пр)',
        'before_widget' => '<div id="%1$s" class="aside-col %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<strong class="aside-title">',
        'after_title' => '</strong>'
    ) );
}
add_action( 'init', 'sidebar_widgets_init' );
