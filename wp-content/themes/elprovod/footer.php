</main>
<footer role="contentinfo">
	<div class="container">
		<div class="block-cols block-cols-4">
			<nav class="block-col">
				<?php
					$theme_locations = get_nav_menu_locations();
					$menu = get_term( $theme_locations['footer-menu-1'], 'nav_menu' );
				?>
				<strong><?php echo $menu->name; ?></strong>
				<?php wp_nav_menu( array(
					'theme_location' => 'footer-menu-1'
				) ); ?>
			</nav>
			<nav class="block-col">
				<?php
					$theme_locations = get_nav_menu_locations();
					$menu = get_term( $theme_locations['footer-menu-2'], 'nav_menu' );
				?>
				<strong><?php echo $menu->name; ?></strong>
				<?php wp_nav_menu( array(
					'theme_location' => 'footer-menu-2'
				) ); ?>
			</nav>
			<nav class="block-col"></nav>
			<aside class="block-col copyrights">
				<strong><?php bloginfo( 'name' ); ?></strong>
				<p>Все права защищены. </p>
				<p>Использование материалов с сайта только с согласия правообладателя.</p>
				<p>&copy; 2002 - 2015</p>
			</aside>
		</div>
	</div>
</footer>
<?php wp_enqueue_script( 'jquery' ); ?>
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/js/main.min.js"></script>
</body>
</html>
