<?php get_header(); ?>

<section <?php post_class( 'category-page single-page' ); ?>>
  <div class="container">
    <section class="wide-col">
      <nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <?php if ( function_exists('bcn_display') ) bcn_display(); ?>
      </nav>
      <h1>Продукция нашего завода</h1>
      <div class="page-content">
        <p>Полный список нашей продукции Вы всегда можете найти <a href="<?php bloginfo( 'home' ); ?>/perechen-produktsii">на этой странице</a>.<br><a href="<?php bloginfo( 'home' ); ?>/wp-content/uploads/2015/11/katalog-produkcii-zao-elektroprovod.pdf">Скачать</a> каталог нашей продукции в формате PDF.</p>
      </div>
      <?php $categories = new WP_Query( array(
        'category_name' => 'bez-rubriki',
        'post_type' => 'product',
        'orderby' => 'menu_order',
        'posts_per_page' => -1
      ) ); ?>
      <?php if ( $categories->have_posts() ) : ?>

      <section class="category-news category-products">
        <?php while ( $categories->have_posts() ) : $categories->the_post(); ?>

        <article <?php post_class( 'news-single product-single' ); ?>>
          <?php if ( has_post_thumbnail() ) : ?>
          <a href="<?php the_permalink(); ?>" class="news-single-img">
            <?php the_post_thumbnail(); ?>
          </a>
          <?php endif; ?>
          <div class="news-single-info">
            <h2 class="news-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          </div>
        </article>

        <?php endwhile; ?>
      </section>

      <?php else : ?>
      <h3 style="color: #e02424;">В этом разделе еще нет записей</h2>
      <?php wp_reset_query(); ?>
      <?php endif; ?>
    </section>

    <?php get_sidebar(); ?>

  </div>
</section>

<?php get_footer(); ?>
