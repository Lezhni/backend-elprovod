<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section <?php post_class( 'single-page product-single-page' ); ?>>
  <div class="container">
    <section class="wide-col">
      <nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <?php if ( function_exists('bcn_display') ) bcn_display(); ?>
      </nav>
      <h1><?php the_title(); ?></h1>
      <?php the_post_thumbnail( array( 'to-left' ) ); ?>
      <section class="page-content">
        <?php the_content(); ?>
      </section>
      <?php $goods = new WP_Query( array(
        'category_name' => $post->post_name,
        'post_type' => 'product',
        'orderby' => 'menu_order'
      ) ); ?>
      <?php if ( $goods->have_posts() ) : ?>

      <section class="category-news category-products">
        <?php while ( $goods->have_posts() ) : $goods->the_post(); ?>

        <article <?php post_class( 'news-single product-single' ); ?>>
          <?php if ( has_post_thumbnail() ) : ?>
          <a href="<?php the_permalink(); ?>" class="news-single-img">
            <?php the_post_thumbnail(); ?>
          </a>
          <?php endif; ?>
          <div class="news-single-info">
            <h2 class="news-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          </div>
        </article>

        <?php endwhile; ?>
      </section>
      
      <?php wp_reset_query(); ?>
      <?php endif; ?>
      <?php
        // Order shortcode
        $order = get_field( 'order_shortcode' );
        if ( ! empty($order) ) : ?>
          <section class="order-form">
            <h3>Оставить заявку на продукцию:</h3>
            <?php echo do_shortcode( $order ); ?>
          </section>
        <?php endif; ?>
    </section>

    <?php get_sidebar(); ?>

  </div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?>
