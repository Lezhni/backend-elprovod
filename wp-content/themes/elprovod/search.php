<?php get_header(); ?>

<section <?php post_class( 'category-page single-page' ); ?>>
  <div class="container">
    <section class="wide-col">
      <nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <?php if ( function_exists('bcn_display') ) bcn_display(); ?>
      </nav>
      <h1>Результаты поиска</h1>
      <?php if ( have_posts() ) : ?>

      <section class="category-news category-search">
        <?php while ( have_posts() ) : the_post(); ?>

        <article <?php post_class( 'news-single' ); ?>>
          <?php if ( has_post_thumbnail() ) : ?>
          <a href="<?php the_permalink(); ?>" class="news-single-img">
            <?php the_post_thumbnail(); ?>
          </a>
          <?php endif; ?>
          <div class="news-single-info">
            <h2 class="news-single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="news-single-desc"><?php the_excerpt(); ?></div>
            <a href="<?php the_permalink(); ?>" class="news-single-more">Читать далее</a>
          </div>
        </article>

        <?php endwhile; ?>
        <?php if ( paginate_links() ) : ?>
        <nav class="pagination"><?php echo paginate_links(); ?></nav>
        <?php endif; ?>
      </section>

      <?php else : ?>
      <h3 style="color: #e02424;">По Вашему запросу ничего не найдено</h2>
      <?php endif; ?>
    </section>

    <?php get_sidebar(); ?>

  </div>
</section>

<?php get_footer(); ?>
