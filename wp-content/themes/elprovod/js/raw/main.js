$ = jQuery;

$(document).on('ready', init);
$(document).on('click', '.contacts-col .button', showForm);

function init() {

	$('.main-slideshow').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: false
	});
}

function showForm() {

	$.magnificPopup.open({
	  items: {
	    src: '#callback',
	    type: 'inline'
	  }
	});

	return false;
}
